<!DOCTYPE html>
<?php
require("class/api.php");
require "methode.php";
 ?>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Sup'avenir</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <script src="https://kit.fontawesome.com/dcd4f7066f.js" crossorigin="anonymous"></script>
</head>
<body>
    <section class="page">
      <h1>Liste des formations</h1>

      <div class="content-inline">
      <div class="side">
          <h3>Préciser mon choix</h3>
          <hr>
          <form action="formations.php" method="post">
          <label for="dom">Choisir un domaine</label>
          <?php
                    $url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=0&sortList=-rentree_lib&facet=etablissement_lib&facet=typ_diplome_lib&facet=sect_disciplinaire_lib&facet=localisation_ins&refine.rentree_lib=2017-18";
                    $contents = file_get_contents($url, true);
                    $results = json_decode($contents, true);
                    $list = sortList($results, 4);
                ?>
                    <select name="dom" id="dom">
            <option value="" selected="selected" disabled="disabled">Domaine</option>
            <?php
              sortedOption($list);
            ?>
            </select>
        <br>
          <label for="form">Choisir une formation</label>
          <?php
                  $list = sortList($results, 2);
                  ?>
        <select name="form" id="form">
          <option value="" selected="selected" disabled="disabled">Formation</option>
            <?php
              sortedOption($list);
            ?>
        </select>
        <br>
          <label for="etab">Choisir un établissement</label>
          <?php
                  $list = sortList($results, 1);
                ?>
          <select name="etab" id="etab">
            <option value="" selected="selected" disabled="disabled">Etablissement</option>
          <?php
              sortedOption($list,"tres");
                ?>
        </select>
        <br>
          <label for="dep">Choisir une région</label>
          <?php
                  $list = sortList($results, 0);
                ?>
          <select name="dep" id="dep">
          <option value="" selected="selected" disabled="disabled">Région</option>
          <?php
              sortedOption($list,"ok");
                ?>
        </select>
        <input class="pulse" type="submit" value="Filtrer" name="Filtrer">
        </form>
      </div>
      <div class="bigside">
          <table>
            <tbody>
                <tr id="table-col-desc">
                    <th>Domaine</th>
                    <th>Type de formation</th>
                    <th>Etablissements</th>
                    <th>Département</th>
                    <th>Carte</th>
                    <th>Plus d'infos</th>
                </tr>
                <?php
                $nb_res = 0;
                  if (isset($_POST["Filtrer"])){
                    $url ="https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=100&facet=etablissement_lib&facet=etablissement&facet=typ_diplome_lib&facet=sect_disciplinaire_lib&facet=localisation_ins&refine.rentree_lib=2017-18";
                    if (isset($_POST["dom"])) {
                      $url .= "&refine.sect_disciplinaire_lib=".$_POST["dom"];
                    }
                    if (isset($_POST["form"])) {
                      $url .= "&refine.typ_diplome_lib=".$_POST["form"];
                    }
                    if (isset($_POST["etab"])) {
                      $url .= "&refine.etablissement_lib=".$_POST["etab"];
                    }
                    if (isset($_POST["dep"])) {
                      $url .= "&refine.localisation_ins=".$_POST["dep"];
                    }
                    $contents = file_get_contents($url, true);
                    $results = json_decode($contents, true);
                    foreach($results["records"] as $ligne) :
                      $nb_res++;
                      echo "<tr><td>".$ligne['fields']['sect_disciplinaire_lib']."</td>";
                      echo "<td>".$ligne['fields']['typ_diplome_lib']."</td>";
                      echo "<td><a href='fiche_etab.php?code=".$ligne['fields']['etablissement']."'>".$ligne['fields']['etablissement_lib']."</a></td>";
                      echo "<td>".$ligne['fields']['dep_etab_lib']."</td>";
                      echo "<td><form action='regions.php' method='post'><select name='eta' class='no-display'><option selected='selected' value='".$ligne['fields']['etablissement']."'>".$ligne['fields']['etablissement']."</option></select><button type='submit' name='card'><i class='fas fa-map-marked-alt'></i></button></form></td>";
                      echo "<td><a href='fiche_form.php?code=".url_encode($ligne['fields']['etablissement'])."&dom=".url_encode($ligne['fields']['sect_disciplinaire_lib'])."&form=".url_encode($ligne['fields']['typ_diplome_lib'])."&annee=".url_encode($ligne['fields']['niveau_lib'])."'><i class='fas fa-info'></i></a></td></tr>";
                    endforeach;
                  }
                  echo "<h3>".$nb_res." résultats trouvés</h3>";
                  echo "<div style='margin-bottom: 1em; text-align: center'><input id='string' name='string' type='text' value='' onfocus='clearSearch()' />
                  <button type='submit' onclick='searchString()'><i class='fas fa-search'></i></button>";
                ?>

            </tbody>
            </table>

      </div>

    </div>
                
      <div class="menu">
        <input class="burger" type="checkbox">
        <nav>
        <div id="page-btn">
        <a class="home-link" href="formations.php">
            <button class="pulse-button">Formations</button></a>
        <a class="home-link" href="regions.php">
            <button class="pulse-button">Carte</button>
          </a>
    </div>
        </nav>
      </div>

      <footer>
        <ul>
          <li><a href="https://bitbucket.org/TeddyOuazzani/opendata/src/master/">Pour en savoir plus</a></li>
        </ul>
        <h5>© Teddy Ouazzani - 2020</h5>
      </footer>
      
      </section>

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script>
function searchString() {
    var Sstring = $('#string').val();
    $("td:contains('" + Sstring + "')").css("background", "var(--page-bg-color");
    var n = $("td:contains('" + Sstring + "')").length;
    alert( n + " occurence(s) trouvée(s)");
    $("td:contains('" + Sstring + "')")[0].scrollIntoView(true);
}
function clearSearch() {
    var Sstring = $('#string').val();
    if (Sstring != '') {
        $("td:contains('" + Sstring + "')").css("background", "none");
        $('#string').val('');
    }
}
</script> 
 <script>

        $(document).ready(function() {
          $("#btn").on("click",function() {
            $("#searchbar").toggle("slow");
            return false;
          });
        });

        $(document).ready(function() {
          $("#btn2").on("click",function() {
            $("form").toggle("slow");
            return false;
          });
        });

</script>
  <script>
    $(function() {
        // Sections height
        $(window).resize(function() {
            var sH = $(window).height();
            $('.page').css('height', sH + 'px');
        });        
    });
  </script>
  </body>
</html>
