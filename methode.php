<?php

function sortList($results, $index){
  $list = array();
  if (isset($results["facet_groups"][$index]["facets"])){
  foreach ($results["facet_groups"][$index]["facets"] as $key => $value) {
    array_push($list, $value["name"]);
  }
}
  asort($list);
  return $list;
  }

function sortedOption($list){
  foreach($list as $value){
    echo "<option value='".$value."'>".$value."</option>";
  }
}

function url_encode($string){
  return urlencode(utf8_encode($string));
}

function url_decode($string){
  return utf8_decode(urldecode($string));
}
 ?>
