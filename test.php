<!DOCTYPE html>
<html lang="fr" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Sup'avenir</title>
        <link rel="stylesheet" href="css/style.css">
    </head>
        <body>
            <?php
                $contents = file_get_contents("base_test.json");
                $contents = utf8_encode($contents);
                $results = json_decode($contents, true);
                $coordjson = file_get_contents("coordonees.json");
                $coordjson = utf8_encode($coordjson);
                $coord = json_decode($coordjson, true); 

                foreach($results as $ligne){
                    $com = $ligne["fields"]["com_etab_lib"];
                    $zip = $ligne["fields"]["com_etab"];
                    print $com . " * " . $zip . " * ";    
                    foreach($coord as $a){
                        if ($a["Code_commune_INSEE"]==$zip){
                            print($a["coordonnees_gps"]);
                            list($coordonnees1, $coordonnees2) = explode(",",$a["coordonnees_gps"]);
                        break;
                        }
                    }
                    echo("<br>");
                }         
            ?>
        </body>
</html>