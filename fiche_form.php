<!DOCTYPE html>
<?php
require "methode.php";
 ?>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Sup'avenir</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
    integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
    crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
    integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
    crossorigin=""></script>
    <script src="https://kit.fontawesome.com/dcd4f7066f.js" crossorigin="anonymous"></script>
  </head>
  <body> 
    <section class="page">
    <h1>Fiche formation</h1>
    <?php
        $code = url_decode($_GET["code"]);
        $dom = url_decode($_GET["dom"]);
        $form = url_decode($_GET["form"]);
        $annee = url_decode($_GET["annee"]);

        $cliquejson = file_get_contents("clique.json");
        $clique = json_decode($cliquejson, true);
        $adresse = $code."_".$dom."_".$form."_".$annee;
        if(isset($clique[$adresse])){
          $clique[$adresse]++;
        }
        else{
          $clique[$adresse] = 1;
        }
        file_put_contents("clique.json",json_encode($clique));

        $url ="https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=1&facet=etablissement_lib&facet=etablissement&facet=typ_diplome_lib&facet=sect_disciplinaire_lib&facet=localisation_ins&refine.rentree_lib=2017-18";
        $url .= "&refine.etablissement=".$code;
        $url .= "&refine.sect_disciplinaire_lib=".$dom;
        $url .= "&refine.typ_diplome_lib=".$form;
        $url .= "&refine.niveau_lib=".$annee;
        $contents = file_get_contents($url, true);
        $results = json_decode($contents, true);
        foreach($results["records"] as $ligne) :
            echo "<h3>".$ligne['fields']['typ_diplome_lib']." ".$ligne['fields']['sect_disciplinaire_lib']." ".$ligne['fields']['etablissement_lib']." (<i class='fas fa-eye'></i> ".$clique[$adresse].")</h3>";
            echo "<p>Libellé du diplome : ".$ligne['fields']['diplome_lib']."</p>";
            echo "<p>Type de diplome : ".$ligne['fields']['dn_de_lib']."</p>";
            echo "<p>Niveau d'étude : ".$ligne['fields']['niveau_lib']."</p>";
            echo "<p>Cycle universitaire : ".$ligne['fields']['cursus_lmd_lib']."</p>";
            if ( (isset($ligne['fields']['hommes'])) && (isset($ligne['fields']['femmes'])) ){
              echo "<p>Effectif : ".$ligne['fields']['effectif_total']." (<i class='fas fa-male'></i> ".$ligne['fields']['hommes']." | <i class='fas fa-female'></i> ".$ligne['fields']['femmes'].")</p>";
            }
            else {
            echo "<p>Effectif : ".$ligne['fields']['effectif_total']." (Répartition homme/femme indisponible)</p>";
            }
          endforeach;
    ?>
    <div class="menu">
        <input class="burger" type="checkbox">
        <nav>
        <div id="page-btn">
        <a class="home-link" href="formations.php">
            <button class="pulse-button">Formations</button></a>
        <a class="home-link" href="regions.php">
            <button class="pulse-button">Carte</button>
          </a>
    </div>
        </nav>
      </div>
      <footer>
        <ul>
          <li><a href="https://bitbucket.org/TeddyOuazzani/opendata/src/master/">Pour en savoir plus</a></li>
        </ul>
        <h5>© Teddy Ouazzani - 2020</h5>
      </footer>
    </section>
    
  </body>
</html>
