<!DOCTYPE html>
<?php
require "methode.php";
 ?>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Sup'avenir</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
    integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
    crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
    integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
    crossorigin=""></script>
    <script src="https://kit.fontawesome.com/dcd4f7066f.js" crossorigin="anonymous"></script>
  </head>
  <body> 
    <section class="page">
    <?php
                    $url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=1&sort=-rentree_lib&facet=etablissement_lib&facet=typ_diplome_lib&facet=disciplines_selection&facet=dep_etab_lib&refine.rentree_lib=2017-18";
                    $url .= "&refine.etablissement=".$_GET["code"];
                    $contents = file_get_contents($url, true);
                    $results = json_decode($contents, true);
                    $url2 ="https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-etablissements-enseignement-superieur&rows=322&sort=uo_lib&facet=uai&facet=type_d_etablissement&facet=com_nom&facet=aca_nom";
                    $url2 .= "&refine.uai=".$_GET["code"];
                    $contents = file_get_contents($url2, true);
                    $results2 = json_decode($contents, true);
                    ?>
                    <h1>Fiche Établissement</h1>
    <div class="content-inline">
        <div class="mid-side">
            <h3>Infos</h3>
            <hr>
            <?php
            $cliquejson = file_get_contents("clique_ext.json");
            $clique = json_decode($cliquejson, true);
            $adresse = $_GET["code"];
            if(isset($clique[$adresse])){
            }
            else{
              $clique[$adresse] = 0;
            }
            file_put_contents("clique_ext.json",json_encode($clique));
            foreach($results2['records'] as $ligne) :
                if (isset($ligne['fields']['sigle'])){
                    echo "<p>Nom de l'établissement : ".$ligne['fields']['uo_lib']." (".$ligne['fields']['sigle'].")</p>";
                }
                else{
                echo "<p>Nom de l'établissement : ".$ligne['fields']['uo_lib']."</p>";
                }
                echo "<p>Type d'établissement : ".$ligne['fields']['type_d_etablissement']."</p>";
                echo "<p>Adresse : ".$ligne['fields']['adresse_uai']."</p>";
                echo "<p>Commune : ".$ligne['fields']['com_nom']."</p>";
                echo "<p>Site web : <a href='redirect.php?&code=".$_GET["code"]."&dir=".$ligne['fields']['url']."'>".$ligne['fields']['url']."</a> (<i class='fas fa-eye'></i> ".$clique[$adresse].")</p>";
            endforeach;
            ?>
            <h3>Formations proposés par l'établissement</h3>
            <table>
            <tbody>
                <tr id="table-col-desc">
                    <th>Domaine</th>
                    <th>Type de formation</th>
                    <th>Niveau</th>
                    <th>Effectif</th>
                    <th>Plus d'infos</th>
                </tr>
                <?php
                $nb_res = 0;
                    $url ="https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=100&facet=etablissement_lib&facet=etablissement&facet=typ_diplome_lib&facet=sect_disciplinaire_lib&facet=localisation_ins&refine.rentree_lib=2017-18";
                      $url .= "&refine.etablissement=".$_GET["code"];
                    $contents = file_get_contents($url, true);
                    $results = json_decode($contents, true);
                    foreach($results["records"] as $ligne) :
                      $nb_res++;
                      echo "<tr><td>".$ligne['fields']['sect_disciplinaire_lib']."</td>";
                      echo "<td>".$ligne['fields']['typ_diplome_lib']."</td>";
                      echo "<td>".$ligne['fields']['niveau_lib']."</td>";
                      if ( (isset($ligne['fields']['hommes'])) && (isset($ligne['fields']['femmes'])) ){
                        echo "<td>Effectif : ".$ligne['fields']['effectif_total']." (<i class='fas fa-male'></i> ".$ligne['fields']['hommes']." | <i class='fas fa-female'></i> ".$ligne['fields']['femmes'].")</td>";  
                    }
                    else {
                    echo "<td>Effectif : ".$ligne['fields']['effectif_total']." (Répartition homme/femme indisponible)</td>";
                    }
                    echo "<td><a href='fiche_form.php?code=".url_encode($ligne['fields']['etablissement'])."&dom=".url_encode($ligne['fields']['sect_disciplinaire_lib'])."&form=".url_encode($ligne['fields']['typ_diplome_lib'])."&annee=".url_encode($ligne['fields']['niveau_lib'])."'><i class='fas fa-info'></i></a></td></tr>";
                    endforeach;
                  echo "<h3>".$nb_res." résultats trouvés</h3>";
                  echo "<div style='margin-bottom: 1em; text-align: center'><input id='string' name='string' type='text' value='' onfocus='clearSearch()' />
                  <button type='submit' onclick='searchString()'><i class='fas fa-search'></i></button>";
                ?>

            </tbody>
            </table>

        </div>
        <div id="mapid" class="mid-side"><div id="load" style="visibility:block;">
      <i class="fas fa-spinner"></i>
</div></div>
    </div>
    <div class="menu">
        <input class="burger" type="checkbox">
        <nav>
        <div id="page-btn">
        <a class="home-link" href="formations.php">
            <button class="pulse-button">Formations</button></a>
        <a class="home-link" href="regions.php">
            <button class="pulse-button">Carte</button>
          </a>
    </div>
        </nav>
      </div>
      <footer>
        <ul>
          <li><a href="https://bitbucket.org/TeddyOuazzani/opendata/src/master/">Pour en savoir plus</a></li>
        </ul>
        <h5>© Teddy Ouazzani - 2020</h5>
      </footer>
    </section>

    <script>
        var load = document.getElementById('load'),
 
window.onload = function(){ // Une fois que le chargement de la page est finie...
    load.style.visibility= "hidden"; // On masque notre div
     
}
      </script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script>
function searchString() {
    var Sstring = $('#string').val();
    $("td:contains('" + Sstring + "')").css("background", "var(--page-bg-color");
    var n = $("td:contains('" + Sstring + "')").length;
    alert( n + " occurence(s) trouvée(s)");
    $("td:contains('" + Sstring + "')")[0].scrollIntoView(true);
}
function clearSearch() {
    var Sstring = $('#string').val();
    if (Sstring != '') {
        $("td:contains('" + Sstring + "')").css("background", "none");
        $('#string').val('');
    }
}
</script> 
    <script>var mymap = L.map('mapid').setView([48.83717499999999, 2.584855800000014], 13);
 L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw'
}).addTo(mymap);
<?php
    $url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=1&facet=etablissement&facet=etablissement_lib&facet=com_etab_lib&facet=dep_etab_lib&refine.rentree_lib=2017-18";
    $url .= "&refine.etablissement=".$_GET["code"];

    $contents = file_get_contents($url, true);
    $results = json_decode($contents, true);
    $coordjson = file_get_contents("coordonees4.json");
    $coord = json_decode($coordjson, true);    

    foreach($results["records"] as $ligne) :
        $com = $ligne["fields"]["com_etab_lib"];
        $eta = $ligne["fields"]["etablissement"];
        $lib = $ligne["fields"]["etablissement_lib"];
        $dep = $ligne["fields"]["dep_etab_lib"];?>
        var com = "<?php echo $com ?>";
        var eta = "<?php echo $eta ?>";
        var lib = "<?php echo $lib ?>";
        <?php

        foreach($coord as $a) :
                        if ($a["fields"]["uai"]==$eta) :
                            $coordonnees1 = $a["fields"]["coordonnees"][0];
                            $coordonnees2 = $a["fields"]["coordonnees"][1];
                            ?>
                            var coordonnees1 = <?php echo $coordonnees1?>;
                            var coordonnees2 = <?php echo $coordonnees2?>;
                            var marker = L.marker([coordonnees1, coordonnees2]).addTo(mymap);
                            marker.bindPopup(lib).openPopup();
                            <?php
                        break;
                        endif ?>
                        
        <?php endforeach; ?>
        
    <?php endforeach; ?>

 </script> 
  </body>
</html>
