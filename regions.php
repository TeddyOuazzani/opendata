<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Sup'avenir</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
    integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
    crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
    integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
    crossorigin=""></script>
    <script src="https://kit.fontawesome.com/dcd4f7066f.js" crossorigin="anonymous"></script>
  </head>
  <body> 
    <section class="page">
      <h1>Carte des formations</h1>
      <div class="content-inline">
      <div class="side">
      <h3>Préciser mon choix</h3>
      <hr>
      <form class="form-map" action="regions.php" method="post">
        <label for="dom">Choisir un domaine</label>
          <select name="dom" id="dom">
            <option value="" selected="selected" disabled="disabled">Domaine</option>
          <?php
                    $url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=0&sort=-rentree_lib&facet=etablissement_lib&facet=typ_diplome_lib&facet=disciplines_selection&facet=dep_etab_lib&refine.rentree_lib=2017-18";
                    $contents = file_get_contents($url, true);
                    $results = json_decode($contents, true);
                    foreach ($results["facet_groups"][4]["facets"] as $key => $value) {
                        echo "<option value='".$value[name]."'>".$value[name]."</option>";
                    }
                ?>
        </select>
        <br>
          <label for="form">Choisir une formation</label>
          <select name="form" id="form">
            <option value="" selected="selected" disabled="disabled">Formation</option>
          <?php
                    foreach ($results["facet_groups"][2]["facets"] as $key => $value) {
                        echo "<option value='".$value[name]."'>".$value[name]."</option>";
                    }
                ?>
        </select>
        <br>
          <label for="etab">Choisir un établissement</label>
          <select name="etab" id="etab">
            <option value="" selected="selected" disabled="disabled">Etablissement</option>
          <?php
                    foreach ($results["facet_groups"][1]["facets"] as $key => $value) {
                        echo "<option value='".$value[name]."'>".$value[name]."</option>";
                    }
                ?>
        </select>
        <br>
          <label for="dep">Choisir une région</label>
          <select name="dep" id="dep">
          <option value="" selected="selected" disabled="disabled">Région</option>
          <?php
                    foreach ($results["facet_groups"][0]["facets"] as $key => $value) {
                        echo "<option value='".$value[name]."'>".$value[name]."</option>";
                    }
                ?>
        </select>
        <input class="pulse" type="submit" value="Filtrer" name="Filtrer">
            </form>
                  </div>
      <div id="mapid" class="bigside">    <div id="load" style="visibility:block;">
      <i class="fas fa-spinner"></i>
</div></div>
    </div>
                  </div>
    <div class="menu">
        <input class="burger" type="checkbox">
        <nav>
        <div id="page-btn">
        <a class="home-link" href="formations.php">
            <button class="pulse-button">Formations</button></a>
        <a class="home-link" href="regions.php">
            <button class="pulse-button">Carte</button>
          </a>
    </div>
        </nav>
      </div>
      <footer>
        <ul>
          <li><a href="https://bitbucket.org/TeddyOuazzani/opendata/src/master/">Pour en savoir plus</a></li>
        </ul>
        <h5>© Teddy Ouazzani - 2020</h5>
      </footer>
    </section>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script>
        var load = document.getElementById('load'),
 
window.onload = function(){ // Une fois que le chargement de la page est finie...
    load.style.visibility= "hidden"; // On masque notre div
     
}
      </script>
 <script>

        $(document).ready(function() {
          $("#btn").on("click",function() {
            $("#searchbar").toggle("slow");
            return false;
          });
        });

        $(document).ready(function() {
          $("#btn2").on("click",function() {
            $("form").toggle("slow");
            return false;
          });
        });

</script>
 <script>var mymap = L.map('mapid').setView([48.83717499999999, 2.584855800000014], 13);
 L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw'
}).addTo(mymap);
<?php
$url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=1000&facet=etablissement&facet=etablissement_lib&facet=com_etab_lib&facet=dep_etab_lib&refine.rentree_lib=2017-18";
if (isset($_POST["Filtrer"])){
  if (isset($_POST["dom"])) {
                      $url .= "&refine.disciplines_selection=".$_POST["dom"];
                    }
                    if (isset($_POST["form"])) {
                      $url .= "&refine.typ_diplome_lib=".$_POST["form"];
                    }
                    if (isset($_POST["etab"])) {
                      $url .= "&refine.etablissement_lib=".$_POST["etab"];
                    }
                    if (isset($_POST["dep"])) {
                      $url .= "&refine.dep_etab_lib=".$_POST["dep"];
                    }
  }
  elseif(isset($_POST["card"])){
    $url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=1&facet=etablissement&facet=etablissement_lib&facet=com_etab_lib&facet=dep_etab_lib&refine.rentree_lib=2017-18";
    $url .= "&refine.etablissement=".$_POST["eta"];
  }

    $contents = file_get_contents($url, true);
    $results = json_decode($contents, true);
    $coordjson = file_get_contents("coordonees4.json");
    $coord = json_decode($coordjson, true);    

    foreach($results["records"] as $ligne) :
        $com = $ligne["fields"]["com_etab_lib"];
        $eta = $ligne["fields"]["etablissement"];
        $lib = $ligne["fields"]["etablissement_lib"];
        $dep = $ligne["fields"]["dep_etab_lib"];?>
        var com = "<?php echo $com ?>";
        var eta = "<?php echo $eta ?>";
        var lib = "<?php echo $lib."<br><a href='fiche_etab.php?code=".$eta."'>Voir la fiche établissement</a>" ?>";
        <?php

        foreach($coord as $a) :
                        if ($a["fields"]["uai"]==$eta) :
                            $coordonnees1 = $a["fields"]["coordonnees"][0];
                            $coordonnees2 = $a["fields"]["coordonnees"][1];
                            ?>
                            var coordonnees1 = <?php echo $coordonnees1?>;
                            var coordonnees2 = <?php echo $coordonnees2?>;
                            var marker = L.marker([coordonnees1, coordonnees2]).addTo(mymap);
                            marker.bindPopup(lib).openPopup();
                            <?php
                        break;
                        endif ?>
                        
        <?php endforeach; ?>
        
    <?php endforeach; ?>

 </script>
  </body>
</html>
