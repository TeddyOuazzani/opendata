<!DOCTYPE html>
<?php
require "methode.php";
 ?>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Sup'avenir</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <script src="https://kit.fontawesome.com/dcd4f7066f.js" crossorigin="anonymous"></script>
  </head>
  <body>
    <section class="home">
    <video autoplay muted loop id="myVideo">
  <source src="img/SA.mp4" type="video/mp4">
</video>
      <div id="home-desc">
        <h1>Sup'Avenir</h1>
        <h3 style="text-align: center">Trouvez la formation du supérieur qui vous correspond</h3>
      </div>
    <div class="top">
      <h3>Les 3 Formations les plus vues</h3>
      <?php
        $max1 = 0;
        $max2 = 0;
        $max3 = 0;

        $cle1= "";
        $cle2= "";
        $cle3= "";

        $dom1= "";
        $dom2= "";
        $dom3= "";

        $form1= "";
        $form2= "";
        $form3= "";

        $annee1="";
        $annee2="";
        $annee3="";

        $cliquejson = file_get_contents("clique.json");
        $clique = json_decode($cliquejson, true);
          foreach($clique as $cle => $valeur){
          if ($valeur > $max3){
            $max1 = $max2;
            $cle1 = $cle2;
            $dom1 = $dom2;
            $form1 = $form2;
            $annee1 = $annee2;

            $max2 = $max3;
            $cle2 = $cle3;
            $dom2 = $dom3;
            $form2 = $form3;
            $annee2 = $annee3;

            $max3 = $valeur;
            $cle3 = (explode("_", $cle)[0]);
            $dom3 = (explode("_", $cle)[1]);
            $form3 = (explode("_", $cle)[2]);
            $annee3 = (explode("_", $cle)[3]);
          }

          elseif ($valeur > $max2 && $valeur < $max3){
            $max1 = $max2;
            $cle1 = $cle2;
            $dom1 = $dom2;
            $form1 = $form2;
            $annee1 = $annee2;

            $max2 = $valeur;
            $cle2 = (explode("_", $cle)[0]);
            $dom2 = (explode("_", $cle)[1]);
            $form2 = (explode("_", $cle)[2]);
            $annee2 = (explode("_", $cle)[3]);
          }

          elseif ($valeur > $max1 && $valeur < $max2){
            $max1 = $valeur;
            $cle1 = (explode("_", $cle)[0]);
            $dom1 = (explode("_", $cle)[1]);
            $form1 = (explode("_", $cle)[2]);
            $annee1 = (explode("_", $cle)[3]);
          }
        }
        $url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=1&sort=-rentree_lib&facet=etablissement_lib&facet=typ_diplome_lib&facet=disciplines_selection&facet=dep_etab_lib&refine.rentree_lib=2017-18";
        $url .= "&refine.etablissement=".$cle3;
        $url .= "&refine.sect_disciplinaire_lib=".$dom3;
        $url .= "&refine.typ_diplome_lib=".$form3;
        $url .= "&refine.niveau_lib=".$annee3;
        
        $contents = file_get_contents($url, true);
        $results = json_decode($contents, true);
        foreach($results["records"] as $ligne) :
        echo "<p>Numéro 1 : <a href='fiche_form.php?code=".url_encode($ligne['fields']['etablissement'])."&dom=".url_encode($ligne['fields']['sect_disciplinaire_lib'])."&form=".url_encode($ligne['fields']['typ_diplome_lib'])."&annee=".url_encode($ligne['fields']['niveau_lib'])."'>".$ligne['fields']['typ_diplome_lib']." ".$ligne['fields']['sect_disciplinaire_lib']." ".$ligne['fields']['etablissement_lib']." (<i class='fas fa-eye'></i> ".$max3.")</a></p>";
        endforeach;

        $url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=1&sort=-rentree_lib&facet=etablissement_lib&facet=typ_diplome_lib&facet=disciplines_selection&facet=dep_etab_lib&refine.rentree_lib=2017-18";
        $url .= "&refine.etablissement=".$cle2;
        $url .= "&refine.sect_disciplinaire_lib=".$dom2;
        $url .= "&refine.typ_diplome_lib=".$form2;
        $url .= "&refine.niveau_lib=".$annee2;

        $contents = file_get_contents($url, true);
        $results = json_decode($contents, true);
        foreach($results["records"] as $ligne) :
        echo "<p>Numéro 2 : <a href='fiche_form.php?code=".url_encode($ligne['fields']['etablissement'])."&dom=".url_encode($ligne['fields']['sect_disciplinaire_lib'])."&form=".url_encode($ligne['fields']['typ_diplome_lib'])."&annee=".url_encode($ligne['fields']['niveau_lib'])."'>".$ligne['fields']['typ_diplome_lib']." ".$ligne['fields']['sect_disciplinaire_lib']." ".$ligne['fields']['etablissement_lib']." (<i class='fas fa-eye'></i> ".$max2.")</a></p>";
        endforeach;
        
        $url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=1&sort=-rentree_lib&facet=etablissement_lib&facet=typ_diplome_lib&facet=disciplines_selection&facet=dep_etab_lib&refine.rentree_lib=2017-18";
        $url .= "&refine.etablissement=".$cle1;
        $url .= "&refine.sect_disciplinaire_lib=".$dom1;
        $url .= "&refine.typ_diplome_lib=".$form1;
        $url .= "&refine.niveau_lib=".$annee1;

        $contents = file_get_contents($url, true);
        $results = json_decode($contents, true);
        foreach($results["records"] as $ligne) :
        echo "<p>Numéro 3 : <a href='fiche_form.php?code=".url_encode($ligne['fields']['etablissement'])."&dom=".url_encode($ligne['fields']['sect_disciplinaire_lib'])."&form=".url_encode($ligne['fields']['typ_diplome_lib'])."&annee=".url_encode($ligne['fields']['niveau_lib'])."'>".$ligne['fields']['typ_diplome_lib']." ".$ligne['fields']['sect_disciplinaire_lib']." ".$ligne['fields']['etablissement_lib']." (<i class='fas fa-eye'></i> ".$max1.")</a></p>";
        endforeach;
      ?>
    </div>
    <div id="home-btn">
        <a class="home-link" href="formations.php">
            <button class="pulse-button">Formations</button></a>
        <a class="home-link" href="regions.php">
            <button class="pulse-button">Carte</button>
          </a>
    </div>
    <footer class="credit">
      <ul style="padding: 0">
          <li><a href="https://bitbucket.org/TeddyOuazzani/opendata/src/master/">Pour en savoir plus</a></li>
      </ul>
        <h5>© Teddy Ouazzani - 2020</h5>
    </footer>
    </section>
  </body>
</html>
